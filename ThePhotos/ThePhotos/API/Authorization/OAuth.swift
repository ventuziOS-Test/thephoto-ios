//
//  OAuth.swift
//  ThePhotos
//
//  Created by Stefano Venturin on 30/11/2017.
//  Copyright © 2017 @ventuz. All rights reserved.
//

import Foundation

final class OAuth {
    
    /// Instagram client
    private let clientID    = "bfea2b98b8034945bd6020e74b203684"
    /// The URI where found the token
    private let redirectURI = "https://localhost/"
    
    /// Compose the URL where connect to.
    /// - returns: URL to connect to perform first login..
    func loginURL() -> URL? {
        var components = URLComponents(string: NetworkUtils.authURL)!
        
        components.queryItems = [
            URLQueryItem(name: "client_id", value: clientID),
            URLQueryItem(name: "redirect_uri", value: redirectURI),
            URLQueryItem(name: "response_type", value: "token")
        ]
        
        return components.url
    }
    
}
