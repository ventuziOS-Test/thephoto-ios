//
//  NetworksUtils.swift
//  ThePhotos
//
//  Created by Stefano Venturin on 30/11/2017.
//  Copyright © 2017 @ventuz. All rights reserved.
//

import Foundation

struct NetworkUtils {
    
    /// URL for OAuth
    static var authURL: String {
        return "https://api.instagram.com/oauth/authorize"
    }
    
    /// Bsse URL for request
    static var baseURL: String {
        return "https://api.instagram.com"
    }
    
    /// The API version to use added to _baseURL_ parameter.
    static var v1: String {
        return baseURL + "/v1"
    }
    
}
