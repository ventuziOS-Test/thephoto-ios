//
//  UserProfile.swift
//  ThePhotos
//
//  Created by Stefano Venturin on 05/12/2017.
//  Copyright © 2017 @ventuz. All rights reserved.
//

import Moya

/// Service that handles user data
enum UserProfile {
    case userProfile(token: String)
    case userRecentMedia(token: String)
}

extension UserProfile: TargetType {
    
    var baseURL: URL {
        return URL(string: NetworkUtils.v1)!
    }
    
    var path: String {
        switch self {
        case .userProfile:
            return "/users/self/"
        case .userRecentMedia:
            return "/users/self/media/recent/"
        }
    }
    
    var method: Method {
        switch self {
        case .userProfile, .userRecentMedia:
            return .get
        }
    }
    
    var task: Task {
        switch self {
        case .userProfile(let token):
            return .requestParameters(parameters: ["access_token": token],
                                      encoding: URLEncoding.queryString)
        case .userRecentMedia(let token):
            return .requestParameters(parameters: ["access_token": token],
                                      encoding: URLEncoding.queryString)
        }
    }
    
    var headers: [String: String]? {
        return ["Content-type": "application/json"]
    }
    
    /// - fill sample data for TDD
    var sampleData: Data {
        return "Half measures are as bad as nothing at all.".utf8Encoded
    }
    
}

// MARK: - Helpers
private extension String {
    var urlEscaped: String {
        return addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    }
    
    var utf8Encoded: Data {
        return data(using: .utf8)!
    }
}
