//
//  AppDelegate.swift
//  ThePhotos
//
//  Created by Stefano Venturin on 30/11/2017.
//  Copyright © 2017 @ventuz. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    /// Stored properties for Autentication
    private let authData = Keychain.shared

    /// Tells the delegate that the launch process is almost done and the app is almost ready to run.
    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

        /// If user is **not** already authenticated, make login.
        if !Keychain.shared.isAuthenticated {
            let loginController = LoginController()
            loginController.setRoot()
        }
        
        return true
    }

}
