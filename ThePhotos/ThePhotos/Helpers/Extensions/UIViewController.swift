//
//  UIViewController.swift
//  ThePhotos
//
//  Created by Stefano Venturin on 05/12/2017.
//  Copyright © 2017 @ventuz. All rights reserved.
//

import UIKit

extension UIViewController {
    
    /// Set the _controller_ as **root** w/ animation
    /// - parameter options: Animation type, part of _UIViewAnimationOptions_
    func setRoot(options: UIViewAnimationOptions = .transitionCrossDissolve) {
        guard let w = UIApplication.shared.delegate?.window, let window = w else { return }
        
        UIView.transition(with: window,
                          duration: 0.5,
                          options: options,
                          animations: {
                            window.rootViewController = self
        })
    }
    
}
