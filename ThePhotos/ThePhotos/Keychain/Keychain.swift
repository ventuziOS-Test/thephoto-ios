//
//  Keychain.swift
//  ThePhotos
//
//  Created by Stefano Venturin on 05/12/2017.
//  Copyright © 2017 @ventuz. All rights reserved.
//

class Keychain {
    
    /// Returns a shared instance of Data.
    static let shared = Keychain()
    
    private enum Keys {
        static let accessTokenKey = "AccessToken"
    }
    
    ///
    private let keychain = KeychainSwift(keyPrefix: "MyToken")
    
    // ================================================
    // MARK: - Access Token Methods
    
    func storeAccessToken(_ accessToken: String) -> Bool {
        return keychain.set(accessToken, forKey: Keys.accessTokenKey)
    }
    
    func retrieveAccessToken() -> String? {
        return keychain.get(Keys.accessTokenKey)
    }
    
    func deleteAccessToken() -> Bool {
        return keychain.delete(Keys.accessTokenKey)
    }
    
    /// Ends the current session.
    /// - returns: True if the user was successfully logged out, false otherwise.
    @discardableResult
    func logout() -> Bool {
        return deleteAccessToken()
    }
    
    /// Returns whether a user is currently authenticated or not.
    var isAuthenticated: Bool {
        return retrieveAccessToken() != nil
    }
    
}
