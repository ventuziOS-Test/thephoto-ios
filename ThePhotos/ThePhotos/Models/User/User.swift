//
//  User.swift
//  ThePhotos
//
//  Created by Stefano Venturin on 05/12/2017.
//  Copyright © 2017 @ventuz. All rights reserved.
//

import ObjectMapper

// ===================================================
// MARk: - Base Response

final class InstaResponse {
    
    var data: User?
    var pagination: Pagination?
    
    struct Pagination: Decodable {
        let nextURL: String?
    }
}

/// Mapping
extension InstaResponse: Mappable {
    
    convenience init?(map: Map) {
        self.init()
    }
    
    /// Map Object
    func mapping(map: Map) {
        data <- map["data"]
        pagination <- map["pagination"]
    }
}


// ===================================================
// MARk: - User

final class User {
    
    /// The user's username.
    var username = ""
    /// /// The URL (String format) of the user's profile picture.
    var profile_picture = ""
    /// A Counts object that contains the number of followers, following and media of a user.
    var counts: Counts?
}

/// Mapping
extension User: Mappable {
    
    convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        profile_picture <- map["profile_picture"]
        username <- map["username"]
        counts <- map["counts"]
    }
}

/// Containing the number of user's media.
final class Counts {
    /// The number of media uploaded by the user.
    var media: Int = 0
}

/// Mapping
extension Counts: Mappable {
    
    convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        media <- map["media"]
    }
}
