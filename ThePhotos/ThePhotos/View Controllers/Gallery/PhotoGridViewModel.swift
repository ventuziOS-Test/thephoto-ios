//
//  PhotoGridViewModel.swift
//  ThePhotos
//
//  Created by Stefano Venturin on 05/12/2017.
//  Copyright © 2017 @ventuz. All rights reserved.
//

import Moya
import ObjectMapper

final class PhotoGridViewModel {
    /// Moya provider to Network requests.
    private let userProvider = MoyaProvider<UserProfile>()
    /// Keychain access class
    private let authData = Keychain.shared
    
    /// User's Instagram data.
    var userObject: User?
    
    // ===========================================
    // MARK: - Methods
    
    /// Get user profile data
    func getUserProfile() {
        guard let token = authData.retrieveAccessToken() else { return }
        
        userProvider.request(.userProfile(token: token)) { [ weak self ] result in
            guard let this = self else { return /* - handle the error */ }

            switch result {
            case let .success(moyaResponse):
                do {
                    let json = try moyaResponse.mapObject(InstaResponse.self)
                    this.userObject = json.data
                } catch {
                    // - handle the error
                    debugPrint("[ ERROR - JSON -> Map User Profile ] - ", error)
                }
            case let .failure(error):
                // - handle the error.
                debugPrint("[ ERROR - Get User Profile ] - ", error)
            }
        }
    }
    
}
