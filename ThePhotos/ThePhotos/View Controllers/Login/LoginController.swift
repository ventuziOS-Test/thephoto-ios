//
//  LoginController.swift
//  ThePhotos
//
//  Created by Stefano Venturin on 30/11/2017.
//  Copyright © 2017 @ventuz. All rights reserved.
//

import UIKit
import WebKit

class LoginController: UIViewController {
    
    /// Model View for Login
    private let loginViewModel = LoginViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadLoginWebView()
    }
    
    // ======================================================
    // MARK: - Methods
    
    private func loadLoginWebView() {
        guard let loginRequestURL = loginViewModel.authURL else {
            // - Handle error for user
            debugPrint("[ERROR - Load Login URL]")
            return
        }
        // Initializes web view
        let webView = setupWebView()
        // Starts authorization
        webView.load(URLRequest(url: loginRequestURL,
                                cachePolicy: .reloadIgnoringLocalAndRemoteCacheData))
    }
    
    /// A view that embeds web content. In this case the Instagram Login.
    private func setupWebView() -> WKWebView {
        let webConfiguration = WKWebViewConfiguration()
        webConfiguration.websiteDataStore = .nonPersistent()
        
        let webView = WKWebView(frame: view.frame, configuration: webConfiguration)
        webView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        webView.navigationDelegate = self

        view.addSubview(webView)
        
        return webView
    }
    
}

// ======================================================
// MARK: - WKNavigationDelegate

extension LoginController: WKNavigationDelegate {
    
    /// Decides whether to allow or cancel a navigation.
    func webView(_ webView: WKWebView,
                 decidePolicyFor navigationAction: WKNavigationAction,
                 decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        
        let urlString = navigationAction.request.url!.absoluteString
        /// Allow to contiune only if exist the access_token
        guard let range = urlString.range(of: "#access_token=") else {
            return decisionHandler(.allow)
        }
        
        decisionHandler(.cancel)
        
        loginViewModel.saveToken(from: String(urlString[range.upperBound...])) { success in
            guard success else {
                return // - Handle error for user
            }
            
            let vc = UIStoryboard.init(name: "PhotoGrid", bundle: nil).instantiateInitialViewController()
            vc?.setRoot()
        }
    }
    
    /// Decides whether to allow or cancel a navigation after its response is known.
    func webView(_ webView: WKWebView,
                 decidePolicyFor navigationResponse: WKNavigationResponse,
                 decisionHandler: @escaping (WKNavigationResponsePolicy) -> Void) {
        guard let httpResponse = navigationResponse.response as? HTTPURLResponse else {
            return decisionHandler(.allow)
        }
        
        switch httpResponse.statusCode {
        case 400:
            decisionHandler(.cancel)
            DispatchQueue.main.async {
                // - Handle error for user
            }
        default:
            decisionHandler(.allow)
        }
    }
    
}
