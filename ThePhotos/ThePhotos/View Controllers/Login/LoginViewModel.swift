//
//  LoginViewModel.swift
//  ThePhotos
//
//  Created by Stefano Venturin on 30/11/2017.
//  Copyright © 2017 @ventuz. All rights reserved.
//

import Foundation

final class LoginViewModel {
    
    ///
    private let oAuth = OAuth()
    /// Stored properties for Autentication
    private let authData = Keychain.shared
    
    init() { /** */ }
    
    // ======================================================
    // MARK: - Methods
    
    /// Get the path to make the authentication.
    var authURL: URL? {
        return oAuth.loginURL()
    }
    
    /// Save the passed token to _keychen_.
    /// - parameter from access_token: The token to save.
    /// - returns: A boolean value that indicate if token is saved correctly.
    func saveToken(from access_token: String, onComplete: @escaping (Bool) -> Void) {
        DispatchQueue.main.async {
            onComplete(self.authData.storeAccessToken(access_token))
        }
    }
    
}
